/**
 * Veritabanı servisi
 * @file index.js
 */

// Database Connection Options
const databaseConnectionOptions = {
    host: process.env.MYSQL_HOST || 'localhost', // Docker servisinizin adı
    database: process.env.MYSQL_DATABASE || 'my_database',
    username: process.env.MYSQL_USER || 'my_user',
    password: process.env.MYSQL_PASSWORD || 'my_password',
}

// OR/M
const { Sequelize, DataTypes } = require('sequelize');
// Veritabanı bağlantısı oluştur
const sequelize = new Sequelize({
    // 'mysql': MySQL veritabanı için.
    // 'postgres': PostgreSQL veritabanı için.
    // 'sqlite': SQLite veritabanı için.
    // 'mssql': Microsoft SQL Server veritabanı için.
    dialect: 'mysql',
    dialectOptions: {
        connectTimeout: 30000, // 30 saniye zaman aşımı ayarı (isteğe bağlı olarak değiştirilebilir)
    },
    host: databaseConnectionOptions.host,
    database: databaseConnectionOptions.database,
    username: databaseConnectionOptions.username,
    password: databaseConnectionOptions.password,
    ssl: false, // Güvenli olmayan bağlantı için SSL'yi devre dışı bırakın
    define: {
        timestamps: false, // Tabloya otomatik olarak createdAt ve updatedAt sütunlarını eklemesini engelle
    },
});

// Todo modeli tanımla
const Todo = sequelize.define('Todo', {
    title: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false,
    },
}, {
    tableName: 'todo', // Tablo adı 'todo' olarak ayarlanır
    timestamps: true, // Zaman damgası alanlarını otomatik olarak ekler
    createdAt: 'created_at', // Oluşturulma zamanı
    updatedAt: 'updated_at', // Güncelleme zamanı    
});
let databaseMigrationStatus = false
function databaseMigration() {
    // Tabloyu oluştur (eğer yoksa)
    sequelize.sync({
        // tabloyu her zaman yeniden oluşturmasını sağlamak için true
        force: false
    })
        .then(() => {
            console.log('ORM : Todo tablosu başarıyla oluşturuldu.');
            databaseMigrationStatus = true

        })
        .catch((error) => {
            console.error('ORM : Tablo oluşturma hatası:', error);
            databaseMigrationStatus = false
        });
};


// Express uygulaması ve rotaları oluştur
const express = require('express');
const app = express();

// Sağlık kontrolü endpoint'i
app.get('/health', (req, res) => {

    if (databaseMigrationStatus) {
        // Tablo oluşturulduğunda 200 OK yanıtını ver
        res.status(200).json({ status: 'OK' });
    } else {
        databaseMigration();
        // Tablo oluşturulmadıysa 500 Internal Server Error yanıtını ver
        res.status(500).json({ error: 'ORM : Tablo oluşturma hatası' });
    }

});

app.get('/', async (req, res) => {
    try {
        // Sunucu adresini alma
        const serverIpAddress = `${req.hostname}:${server.address().address}:${server.address().port}`;
        // İstemci IP adresi
        const clientIpAddress = `${req.ip}:${req.socket.remotePort}`;
        // Veritabanına yazma işlemi
        const createdTodo = await Todo.create({

            title: `${serverIpAddress}`,
            description: `${clientIpAddress}`,
        });

        console.log("Api : /create : ", createdTodo)

        // Veritabanından okuma işlemi
        const todos = await Todo.findAll();

        // Sonuçları JSON olarak gönder
        res.json(todos);
    } catch (error) {
        console.error('Api : işlem hatası:', error);
        res.status(500).json({
            error: 'Api : işlem hatası',
            detail: {
                serverIpAddress, clientIpAddress, error
            }
        });
    }
});

// Api sunucu

// Üçüncü argüman (index 2) port numarasını içerir.
const portArg = process.argv[2];

// Eğer argüman yoksa varsayılan olarak ortam değişkeni kullan.
const portNumber = portArg || process.env.PORT;

// Eğer herhangi bir tanım yok ise, varsayılan 3000 portu ile başla.
const port = portNumber || 3000;
const server = app.listen(port, '0.0.0.0', () => {
    console.log(`Sistem : Sunucu ${port} portunda çalışıyor.`);
    console.log('Sistem : Database bağlantı bilgileri', databaseConnectionOptions)
});
