# Dockerfile
# Resmi Node.js görüntüsünü temel alın
FROM node:14

# Uygulama kodunu çalıştıracağınız bir çalışma dizini oluşturun
WORKDIR /app

# Uygulama bağımlılıklarını kopyalayın ve yükleyin
COPY package*.json ./
RUN npm install

# Uygulama kodunu kopyalayın
COPY . .

# Uygulamayı başlatın ve 3000 numaralı portu dinleyin
EXPOSE 3000

# Uygulamayı başlatın
CMD ["npm", "start"]
